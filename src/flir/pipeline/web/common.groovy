#!/bin/groovy
package flir.pipeline.web

def environment(productName, majorVer, minorVer){
    env.BUILD_PRODUCTNAME = productName
    env.BUILD_MAJOR_VERSION = majorVer
    env.BUILD_MINOR_VERSION = minorVer
    env.BUILD_COMPANY = "${version_web.Company()}"
    env.BUILD_TRADEMARK = "${version_web.TradeMark()}"
    env.BUILD_STABILITY = "${version_web.getStabilityLevel("${env.BRANCH_NAME}")}"
    env.BUILD_CURRENTNUM = VersionNumber( projectStartDate: '', versionNumberString: '${BUILDS_TODAY}', versionPrefix: "", worstResultForIncrement: 'SUCCESS' )
    env.BUILD_BRANCHLEVEL = "${version_web.branchLevel("${env.BRANCH_NAME}")}"
    env.BUILD_REVISION = "${version_web.dayOfYear()}"
    env.BUILD_VERSION = "${version_web.buildNum("${env.BRANCH_NAME}", majorVer, minorVer, "${env.BUILD_CURRENTNUM}")}"
    //env.BUILD_REPO_URL="${env.GIT_URL}"
    //env.BUILD_REPOR_SLUG = "${version_web.repoSlug("${env.GIT_URL}")}"
}

def begin(){
    bat "echo common:begin"
    //bitbucketStatusNotify(buildState: 'INPROGRESS', commitId: "${env.GIT_COMMIT}")
    bat script: "./env.bat"
}

def setVersion(){
    bat "echo common:setVersion"
    powershell ".\\common\\pipeline\\Set-Version.ps1 ${env.BUILD_VERSION} ${env.GIT_COMMIT}"
}

def setServiceWorkerVersion(){
    bat "echo common:setServiceWorkerVersion"
    powershell ".\\common\\pipeline\\Set-ServiceWorkerVersion.ps1 ${env.BUILD_VERSION} ${env.GIT_COMMIT}"
}

def runDeploy(){
    bat "echo common:runDeploy"
    powershell ".\\common\\pipeline\\Run-Deploy.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}




return this
