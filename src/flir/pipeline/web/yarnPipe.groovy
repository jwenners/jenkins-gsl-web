#!/bin/groovy
package flir.pipeline.web

def setup(){
    bat "echo yarnPipe:setup"
    powershell ".\\common\\pipeline\\Install-Yarn.ps1"
}
def newInstall(){
    bat "echo yarnPipe:newInstall"
    powershell ".\\common\\pipeline\\New-Install.ps1"
}
def runBuild(){
    bat "echo yarnPipe:runBuild"
    powershell ".\\common\\pipeline\\Run-Build.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}
def runTest(){
    bat "echo yarnPipe:runTest"
    powershell ".\\common\\pipeline\\Run-Test.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}
def runDeploy(){
    bat "echo common:runDeploy"
    powershell ".\\common\\pipeline\\Run-Deploy.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}

return this