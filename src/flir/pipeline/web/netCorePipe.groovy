#!/bin/groovy
package flir.pipeline.web

def setup(){
    bat "echo netCorePipe:setup"
    powershell ".\\common\\pipeline\\net-core\\Install-NetCore.ps1"
    bat script: "./env.bat"
}
def newInstall(){
    bat "echo netCorePipe:newInstall"
    powershell ".\\common\\pipeline\\net-core\\New-Install.ps1"
}
def runBuild(){
    bat script: "./env.bat"
    bat "echo netCorePipe:runBuild"
    powershell ".\\common\\pipeline\\net-core\\Run-Build.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}
def runTest(){
    bat "echo netCorePipe:runTest"
    powershell ".\\common\\pipeline\\net-core\\Run-Test.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}
def runDeploy(){
    bat "echo common:runDeploy"
    powershell ".\\common\\pipeline\\net-core\\Run-Deploy.ps1 -BranchLevel ${env.BUILD_BRANCHLEVEL}.ps1"
}

return this