param (
    [string]$BranchLevel = "feature.ps1"
)

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))

. "$currentPath\functions.ps1"
. "$rootPath\params\$BranchLevel"

if (!$cmdTest) {
    $cmdTest = "(npm run build $rootPath)"
    write-output "set cmdTest to defalt value = $cmdTest"
}

Run-CommandLine "(npm run build $rootPath)"

