param (
    [string]$BranchLevel = "feature.ps1",
    [string]$Channel = "2.2",
    [string]$Architecture = "x86",
    [string]$framework = "netcoreapp2.2",
    [string]$configuration = "Release",
    [string]$Project
)

" --- BUILD START ---"
write-output "BranchLevel: $BranchLevel"
write-output "Channel: $Channel"
write-output "Architecture: $Architecture"
write-output "framework: $framework"
write-output "configuration: $configuration"
write-output "Project: $Project"


$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))

write-output "currentPath: $currentPath"
write-output "rootPath: $rootPath"

. "$currentPath\functions.ps1"
. "$rootPath\params\$BranchLevel"

# Installation requires ADMIN to test on your computer
write-output "dotnetinstall.ps1"
Run-CommandLine "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
write-output "(dotnet --info)"
Run-CommandLine "(dotnet --info)"
write-output "(dotnet publish $rootPath/$Project --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --source https://api.nuget.org/v3/index.json  --packages deps --framework $framework --configuration $configuration --verbosity normal)"
Run-CommandLine "(dotnet publish $rootPath/$Project --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --source https://api.nuget.org/v3/index.json  --packages deps --framework $framework --configuration $configuration --verbosity normal)"

" --- BUILD END ---"