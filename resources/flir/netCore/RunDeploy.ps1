param (
    [string]$BranchLevel = "feature.ps1",
    [string]$ZipPath
)

" --- DEPLOY START ---"

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))

if (Test-Path "$rootPath\params\$BranchLevel") {
    write-output "Set paramaters..."
    . "$currentPath\functions.ps1"
    . "$rootPath\params\$BranchLevel"
    write-output ""
    write-output "Running deployment pipline for -Param $BranchLevel" 
} else {
    write-output "The given paramater -Param $BranchLevel does not have a corresponding deployment pipeline."
    Exit(1)
}

$pathToZip = "$rootPath/$ZipPath"
$zipFile = "$rootPath/deploy.zip"

Deploy-Zip $pathToZip $zipFile $excludeFiles $username $password $apiUrl
write-output "End"

" --- DEPLOY END ---"
