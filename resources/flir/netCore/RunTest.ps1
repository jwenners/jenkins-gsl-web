param (
    [string]$BranchLevel = "feature.ps1",
    [string]$Channel = "2.2",
    [string]$Architecture = "x86",
    [string]$configuration = "Release",
    [string]$Project
)

" --- TEST START ---"
write-output "BranchLevel: $BranchLevel"
write-output "Channel: $Channel"
write-output "Architecture: $Architecture"
write-output "configuration: $configuration"
write-output "Project: $Project"

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$currentPath
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))
$rootPath

write-output "currentPath: $currentPath"
write-output "rootPath: $rootPath"

. "$currentPath\functions.ps1"
. "$rootPath\params\$BranchLevel"

# Installation requires ADMIN to test on your computer
write-output "($currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture)"
Run-CommandLine "($currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture)"
Write-Output "(dotnet build $rootPath\$Project --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --source https://api.nuget.org/v3/index.json --packages deps)"
Run-CommandLine "(dotnet build $rootPath\$Project --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --source https://api.nuget.org/v3/index.json --packages deps)"
write-output "(dotnet test $rootPath\$Project --configuration $configuration --no-restore --logger 'trx;logfilename=tests.trx')"
Run-CommandLine "(dotnet test $rootPath\$Project --configuration $configuration --no-restore --logger 'trx;logfilename=tests.trx')"

" --- TEST END ---"