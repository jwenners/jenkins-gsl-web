$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))

. "$currentPath\functions.ps1"
Run-CommandLine "npm install -g yarn"
Run-CommandLine "yarn cache clean"
Run-CommandLine "yarn"