# https://docs.microsoft.com/en-us/azure/app-service/app-service-deploy-zip

function Create-Zip($pathToZip, $zipFile, $excludeFiles) {
   
    if (Test-Path $zipFile) {
        Remove-Item $zipFile
    }
    # get files to compress using exclusion filer
    write-output ""
    write-output "Get-ChildItem -Path $pathToZip -Exclude $excludeFiles"
    $files = Get-ChildItem -Path $pathToZip -Exclude $excludeFiles 
    # compress
    write-output ""
    write-output "Compress-Archive -Path $files -DestinationPath $zipFile -CompressionLevel Fastest"
    Compress-Archive -Path $files -DestinationPath $zipFile -CompressionLevel Fastest 
}

function Write-Zip($username, $password, $zipFile, $apiUrl) {
    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $username, $password)))
    $userAgent = "powershell/1.0"
    
    write-output ""
    write-output "Inovoke-RestMetod -Uri $apiUrl -UserAgent $userAgent -Method POST -InFile $zipFile -ContentType multipart/form-data"
    Invoke-RestMethod -Uri $apiUrl -Headers @{Authorization=("Basic {0}" -f $base64AuthInfo)} -UserAgent $userAgent -Method POST -InFile $zipFile -ContentType "multipart/form-data" 
}

function Deploy-Zip($pathToZip, $zipFile, $excludeFiles, $username, $password, $apiUrl) {
    Create-Zip $pathToZip $zipFile $excludeFiles
    Write-Zip $userName $password $zipFile $apiUrl 
}

function Run-CommandLine($cmd) {
    try {
        Write-Output "Command: $cmd"
        Get-ChildItem "."
        Invoke-Expression $cmd
    } catch {
        Throw "Program failed."
    }
}