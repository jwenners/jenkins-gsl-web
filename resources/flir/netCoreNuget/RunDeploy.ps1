param (
    [string]$NugetServer,
    [string]$NugetPath,
    [string]$ApiKey,
    [string]$Channel = "2.2",
    [string]$Architecture = "x86"
)

" --- DEPLOY START ---"

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))


. "$currentPath\functions.ps1"
write-output "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
Run-CommandLine "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
Write-Output "(dotnet nuget push $rootPath/$NugetPath -k $ApiKey -s $NugetServer)"
Run-CommandLine "(dotnet nuget push $rootPath/$NugetPath -k $ApiKey -s $NugetServer)"
#Invoke-Expression "(dotnet nuget push $rootPath\$NugetPath -k oy2bsfuvjnb3aswlsb2qb34ndd62oniu7exfxouxhvvjam -s https://flir-nuget-gallery.azurewebsites.net/api/v2/package)"
write-output "End"

" --- DEPLOY END ---"
