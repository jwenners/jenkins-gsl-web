param (
    [string]$Channel = "2.2",
    [string]$Architecture = "x86"
)

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))

" --- SETUP START ---"
write-output "currentPath: $currentPath"
. "$currentPath\functions.ps1"

# Installation requires ADMIN to test on your computer
Run-CommandLine "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
Run-CommandLine "(dotnet --info)"

" --- SETUP END ---"