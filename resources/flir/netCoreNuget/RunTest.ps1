param (
    [string]$Channel = "2.2",
    [string]$Architecture = "x86",
    [string]$configuration = "Release",
    [string]$Project,
    [string]$LocalPackages
)

" --- TEST START ---"
write-output "Channel: $Channel"
write-output "Architecture: $Architecture"
write-output "configuration: $configuration"
write-output "Project: $Project"
write-output "LocalPackages: $LocalPackages"

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$currentPath
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))
$rootPath

write-output "currentPath: $currentPath"
write-output "rootPath: $rootPath"

$PackagesRef = $LocalPackages.Replace('__rootpath__', $rootPath)
write-output "PackagesRef: $PackagesRef"

. "$currentPath\functions.ps1"

# Installation requires ADMIN to test on your computer
write-output "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
Run-CommandLine "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
Write-Output "(dotnet build $rootPath\$Project $PackagesRef --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --source https://api.nuget.org/v3/index.json --packages deps)"
Run-CommandLine "(dotnet build $rootPath\$Project $PackagesRef --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --source https://api.nuget.org/v3/index.json --packages deps)"
write-output "(dotnet test $rootPath\$Project --configuration $configuration --no-restore --logger 'trx;logfilename=tests.trx')"
Run-CommandLine "(dotnet test $rootPath\$Project --configuration $configuration --no-restore --logger 'trx;logfilename=tests.trx')"

" --- TEST END ---"