param (
    [string]$BranchLevel = "feature.ps1",
    [string]$Channel = "2.2",
    [string]$Architecture = "x86",
    [string]$framework = "netcoreapp2.2",
    [string]$configuration = "Release",
    [string]$Project,
    [string]$version,
    [string]$LocalPackages
)

" --- BUILD START ---"
write-output "BranchLevel: $BranchLevel"
write-output "Channel: $Channel"
write-output "Architecture: $Architecture"
write-output "framework: $framework"
write-output "configuration: $configuration"
write-output "Project: $Project"
write-output "Version: $version"
write-output "LocalPackages: $LocalPackages"


$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))

write-output "currentPath: $currentPath"
write-output "rootPath: $rootPath"

$PackagesRef = $LocalPackages.Replace('__rootpath__', $rootPath)
write-output "PackagesRef: $PackagesRef"

. "$currentPath\functions.ps1"

# Installation requires ADMIN to test on your computer
write-output "dotnetinstall.ps1"
Run-CommandLine "$currentPath\dotnetinstall.ps1 -Channel $Channel -Architecture $Architecture"
write-output "(dotnet --info)"
Run-CommandLine "(dotnet --info)"
write-output "(dotnet build $rootPath/$Project --framework $framework --source https://api.nuget.org/v3/index.json $PackagesRef --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --ignore-failed-sources --configuration $configuration --verbosity normal -p:Version=$version)"
Run-CommandLine "(dotnet build $rootPath/$Project --framework $framework --source https://api.nuget.org/v3/index.json $PackagesRef --source https://flir-nuget-gallery.azurewebsites.net/api/v2/ --ignore-failed-sources --configuration $configuration --verbosity normal -p:Version=$version)"

" --- BUILD END ---"