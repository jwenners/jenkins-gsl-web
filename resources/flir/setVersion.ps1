param (
    [string]$version,
    [string]$gitCommit 
)

" --- VERSION START ---"

if(-not($version)) { Throw "You must supply a value for -Version" }
if(-not($gitCommit)) { Throw "You must supply a value for -GitCommit" }

$currentPath = $PSCommandPath.Substring(0, $PSCommandPath.lastIndexOf('\'))
#$pipelinePath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))
#$commonPath = $pipelinePath.Substring(0, $pipelinePath.lastIndexOf('\'))
$rootPath = $currentPath.Substring(0, $currentPath.lastIndexOf('\'))

$versionFile = "$rootPath\version.json"

if (-not (Test-Path $versionFile)) {
    Throw "$versionFile does not exist."
} 
Write-Output "Verions File: " $versionFile
$s = Get-Content $versionFile -Raw|ConvertFrom-Json
write-output "Verions File Pre: $s"

$s.VERSION=$version
$s.GIT_COMMIT=$gitCommit
$s|ConvertTo-Json |Set-Content $versionFile

write-output "Version File Post: $s"

" --- VERSION END ---"