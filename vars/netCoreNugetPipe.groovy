// vars/netCoreNugetPipe.groovy

def call(String agentLabel, String netCoreChannel, String netCoreArchitecture, String netCoreframework, String buildConfiguration, String[] buildProjects, String[] testProjects, String[] nugetPaths, String nugetServer, String apiKey, String localPackages, String majorVersion, String minorVersion, String buildTimeout, String testTimeout, String productName ,Closure setEnv){
    pipeline{
        agent{
            label agentLabel
        }
        options {
            buildDiscarder(logRotator(numToKeepStr: '6', artifactNumToKeepStr: '6'))
            skipStagesAfterUnstable()
            timestamps()
        }
        environment {
            BUILD_REPO_URL="${env.GIT_URL}"
            BUILD_REPO_SLUG = version_web.repoSlug("${env.GIT_URL}")
            BUILD_PRODUCTNAME = "${productName}"
            BUILD_MAJOR_VERSION = "${majorVersion}"
            BUILD_MINOR_VERSION = "${minorVersion}"
            BUILD_COMPANY = "FLIR Systems"
            BUILD_TRADEMARK = "FLIR® is a registered trademark of FLIR Systems."
            BUILD_STABILITY = "${version_web.getStabilityLevel("${env.BRANCH_NAME}")}"
            BUILD_CURRENTNUM = VersionNumber( projectStartDate: '', versionNumberString: '${BUILDS_TODAY}', versionPrefix: "", worstResultForIncrement: 'SUCCESS' )
            BUILD_BRANCHLEVEL = "${version_web.branchLevel("${env.BRANCH_NAME}")}"
            BUILD_REVISION = "${version_web.dayOfYear()}"
            BUILD_VERSION = "${version_web.buildNum("${env.BRANCH_NAME}", majorVersion, minorVersion, "${env.BUILD_CURRENTNUM}")}"
            BUILD_VERSION_NET = "${version_web.buildNumNet("${env.BUILD_VERSION}", "${env.BRANCH_NAME}")}"
        }
        stages{
            stage('pre-build'){
                steps{
                    script{
                        if(fileExists('./customScript/preBegin.ps1')){
                            powershell '.\\customScript\\preBegin.ps1'
                        }
                    }
                    bat "set"
                    bitbucketStatusNotify(buildState: 'INPROGRESS', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
                    bat 'mkdir .\\tmpScripts'
                    script{
                        def script_functions = libraryResource 'flir/functions.ps1'
                        writeFile file: './tmpScripts/functions.ps1', text: script_functions
                        def script_dotnetinstall = libraryResource 'flir/netCore/dotnetinstall.ps1'
                        writeFile file: './tmpScripts/dotnetinstall.ps1', text: script_dotnetinstall
                        def script_installNetCore = libraryResource 'flir/netCore/InstallNetCore.ps1'
                        writeFile file: './tmpScripts/InstallNetCore.ps1', text: script_installNetCore
                    }
                    powershell "./tmpScripts/InstallNetCore.ps1 ${netCoreChannel} ${netCoreArchitecture}"
                    script{
                        if(fileExists('./customScript/postBegin.ps1')){
                            powershell '.\\customScript\\postBegin.ps1'
                        }
                    }        
                }
            }
            stage('build'){
                steps{
                    script{
                        if(fileExists('./customScript/preBuild.ps1')){
                            powershell '.\\customScript\\preBuild.ps1'
                        }
                    }
                    script{
                        def script_setVersion = libraryResource 'flir/netCore/setVersion.ps1'
                        writeFile file: './tmpScripts/setVersion.ps1', text: script_setVersion
                        def script_runBuild = libraryResource 'flir/netCoreNuget/RunBuild.ps1'
                        writeFile file: './tmpScripts/RunBuild.ps1', text: script_runBuild
                    }
                    timeout(time: buildTimeout, unit: 'MINUTES'){
                        script{
                            for(buildProject in buildProjects){
                                powershell ".\\tmpScripts\\RunBuild.ps1 '${env.BUILD_BRANCHLEVEL}.ps1' '${netCoreChannel}' '${netCoreArchitecture}' '${netCoreframework}' '${buildConfiguration}' '${buildProject}' '${BUILD_VERSION_NET}' '${localPackages}'"
                            }
                        }
                    }
                    script{
                        if(fileExists('./customScript/postBuild.ps1')){
                            powershell '.\\customScript\\postBuild.ps1'
                        }
                    } 
                }
            }
            stage('test'){
                steps{
                    script{
                        if(fileExists('./customScript/preTest.ps1')){
                            powershell '.\\customScript\\preTest.ps1'
                        }
                    }
                    script{
                        def script_runTest = libraryResource 'flir/netCoreNuget/RunTest.ps1'
                        writeFile file: './tmpScripts/RunTest.ps1', text: script_runTest
                    }
                    timeout(time: testTimeout, unit: 'MINUTES'){
                        script{
                            for(testProject in testProjects){
                                powershell ".\\tmpScripts\\RunTest.ps1 '${netCoreChannel}' '${netCoreArchitecture}' '${buildConfiguration}' '${testProject}' '${localPackages}'"
                            }
                        }
                    }
                    script{
                        if(fileExists('./customScript/postTest.ps1')){
                            powershell '.\\customScript\\postTest.ps1'
                        }
                    } 
                }
            }
            stage('deploy'){
                steps{
                    script{
                        if(fileExists('./customScript/preDeploy.ps1')){
                            powershell '.\\customScript\\preDeploy.ps1'
                        }
                    }
                    script{
                        def script_runDeploy = libraryResource 'flir/netCoreNuget/RunDeploy.ps1'
                        writeFile file: './tmpScripts/RunDeploy.ps1', text: script_runDeploy
                    }
                    script{
                        for (nugetPath in nugetPaths){
                            def filePath = findFiles glob: "${nugetPath}"
                            echo "filePath: ${filePath[0]}"
                            powershell ".\\tmpScripts\\RunDeploy.ps1 '${nugetServer}' '${filePath[0]}' '${apiKey}' '${netCoreChannel}' '${netCoreArchitecture}'"
                        }
                    }
                    script{
                        if(fileExists('./customScript/postDeploy.ps1')){
                            powershell '.\\customScript\\postDeploy.ps1'
                        }
                    } 
                }
            }
        }
        post{
            always{
                script{
                    for (nugetPath in nugetPaths){
                        echo "${nugetPath}"
                        archiveArtifacts artifacts: "${nugetPath}", fingerprint: true
                    }
                }
            }
            success{
                bitbucketStatusNotify(buildState: 'SUCCESSFUL', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
            }
            failure{
                bitbucketStatusNotify(buildState: 'FAILED', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
            }
        }
    }
}
