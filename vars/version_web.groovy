def Company(){
    return "FLIR Systems"
}

def TradeMark(){
    return "FLIR® is a registered trademark of FLIR Systems."
}

def dayOfYear() {
  return new Date().format("yyDDD")
}

def buildNum(bname, major, minor, builds){
  def verPrefix = major + "." + minor + "." + dayOfYear() + "."
  if (isMasterBranch(bname))  {
    return verPrefix + sprintf("1%03d", builds.toInteger())
  }
  else if (isReleaseBranch(bname))  {
    return verPrefix + sprintf("2%03d", builds.toInteger())
  }
  else if (isDevelopBranch(bname))  {
    return verPrefix + sprintf("1%02d", builds.toInteger())
  }
  else  {
    return verPrefix + builds
  }
}

def buildNumNet(version, bname){
  if (isMasterBranch(bname))  {
    return version
  }
  else if (isReleaseBranch(bname))  {
    return version
  }
  else if (isDevelopBranch(bname))  {
    return version + "-beta"
  }
  else  {
    return version + "-alpha-" +  branchStrippedName(bname)
  }
}

def branchStrippedName(String bname)
{
  bat "echo Branch Name: ${bname}"
  String[] sbname = bname.split("/")
  bat "echo Branch Name array: ${sbname.last()}"
  return sbname.last()
}

def isFeatureBranch(bname){
  return (bname.indexOf("feature") == 0)
}

def isDevelopBranch(bname){
  return (bname.indexOf("develop") == 0)
}

def isMasterBranch(bname){
  return (bname.indexOf("master") == 0)
}

def isReleaseBranch(bname){
  return (bname.indexOf("release") == 0)
}

def branchLevel(bname){
  if (isFeatureBranch(bname)){
    return "feature"
  }
  if (isDevelopBranch(bname)){
    return "develop"
  }
  if (isMasterBranch(bname)){
    return "master"
  }
  if (isReleaseBranch(bname)){
    return "release"
  }
  return "other"
}

def getStabilityLevel(bname){
  if (isMasterBranch(bname))
    return "3"
  else if (isReleaseBranch(bname))
    return "4"
  else if (isDevelopBranch(bname))
    return "2"
  else
    return "1"
}

def repoSlug(repoUrl)
{
  return repoUrl.split("/")[-1].split("\\.")[0]
}