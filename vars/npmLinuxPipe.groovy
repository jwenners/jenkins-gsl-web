// vars/npmLinuxPipe.groovy

def call(String agentLabel, String npmTool,String productName, String majorVersion, String minorVersion, int buildTimeout, int testTimeout ,Closure setEnv){
    pipeline{
        agent{
            label agentLabel
        }
        tools {nodejs "${npmTool}"}
        options {
            buildDiscarder(logRotator(numToKeepStr: '6', artifactNumToKeepStr: '6'))
            skipStagesAfterUnstable()
            timestamps()
        }
        environment {
            BUILD_REPO_URL="${env.GIT_URL}"
            BUILD_REPO_SLUG = version_web.repoSlug("${env.GIT_URL}")
            BUILD_PRODUCTNAME = "${productName}"
            BUILD_MAJOR_VERSION = "${majorVersion}"
            BUILD_MINOR_VERSION = "${minorVersion}"
            BUILD_COMPANY = "FLIR Systems"
            BUILD_TRADEMARK = "FLIR® is a registered trademark of FLIR Systems."
            BUILD_STABILITY = "${version_web.getStabilityLevel("${env.BRANCH_NAME}")}"
            BUILD_CURRENTNUM = VersionNumber( projectStartDate: '', versionNumberString: '${BUILDS_TODAY}', versionPrefix: "", worstResultForIncrement: 'SUCCESS' )
            BUILD_BRANCHLEVEL = "${version_web.branchLevel("${env.BRANCH_NAME}")}"
            BUILD_REVISION = "${version_web.dayOfYear()}"
            BUILD_VERSION = "${version_web.buildNum("${env.BRANCH_NAME}", majorVersion, minorVersion, "${env.BUILD_CURRENTNUM}")}"
        }
        stages{
            stage('pre-build'){
                steps{
                    // script{
                    //     if(fileExists('./customScript/preBegin.sh')){
                    //         powershell '.\\customScript\\preBegin.sh'
                    //     }
                    // }
                    sh "export"
                    bitbucketStatusNotify(buildState: 'INPROGRESS', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
                    sh "mkdir ${env.WORKSPACE}/tmpScripts"
                    sh "chmod +rwx ${env.WORKSPACE}/tmpScripts"
                    script{
                        setEnv()
                        // def script_functions = libraryResource 'flir/functions.sh'
                        // writeFile file: './tmpScripts/functions.sh', text: script_functions
                        // def script_setVersion = libraryResource 'flir/setVersion.sh'
                        // writeFile file: './tmpScripts/setVersion.sh', text: script_setVersion
                    }
                    // script{
                    //     if(fileExists('./customScript/postBegin.sh')){
                    //         powershell '.\\customScript\\postBegin.sh'
                    //     }
                    // }        
                }
            }
            stage('build'){
                steps{
                    // script{
                    //     if(fileExists('./customScript/preBuild.sh')){
                    //         powershell '.\\customScript\\preBuild.sh'
                    //     }
                    // }
                    script{
                        def script_NewInstall = libraryResource 'flir/npm/NewInstall.sh'
                        writeFile file: "${env.WORKSPACE}/tmpScripts/NewInstall.sh", text: script_NewInstall
                        sh "chmod +x ${env.WORKSPACE}/tmpScripts/NewInstall.sh"
                        def script_runBuild = libraryResource 'flir/npm/RunBuild.sh'
                        writeFile file: "${env.WORKSPACE}/tmpScripts/RunBuild.sh", text: script_runBuild
                        sh "chmod +x ${env.WORKSPACE}/tmpScripts/RunBuild.sh"
                    }
                    timeout(time: buildTimeout, unit: 'MINUTES'){
                        //powershell ".\\tmpScripts\\setVersion.sh ${env.BUILD_VERSION} ${env.GIT_COMMIT}"
                        sh "ls -l"
                        sh "ls -l ${env.WORKSPACE}/tmpScripts"
                        sh "${env.WORKSPACE}/tmpScripts/NewInstall.sh"
                        sh "${env.WORKSPACE}/tmpScripts/RunBuild.sh '${env.BUILD_BRANCHLEVEL}.sh'"
                    }
                    // script{
                    //     if(fileExists('./customScript/postBuild.sh')){
                    //         powershell '.\\customScript\\postBuild.sh'
                    //     }
                    // } 
                }
            }
            // stage('test'){
            //     steps{
            //         script{
            //             if(fileExists('./customScript/preTest.sh')){
            //                 powershell '.\\customScript\\preTest.sh'
            //             }
            //         }
            //         script{
            //             def script_runTest = libraryResource 'flir/npm/RunTest.sh'
            //             writeFile file: './tmpScripts/RunTest.sh', text: script_runTest
            //         }
            //         timeout(time: testTimeout, unit: 'MINUTES'){
            //             powershell ".\\tmpScripts\\RunTest.sh '${env.BUILD_BRANCHLEVEL}.sh'"
            //         }
            //         script{
            //             if(fileExists('./customScript/postTest.sh')){
            //                 powershell '.\\customScript\\postTest.sh'
            //             }
            //         } 
            //     }
            // }
            // stage('deploy'){
            //     steps{
            //         script{
            //             if(fileExists('./customScript/preDeploy.sh')){
            //                 powershell '.\\customScript\\preDeploy.sh'
            //             }
            //         }
            //         script{
            //             def script_runDeploy = libraryResource 'flir/npm/RunDeploy.sh'
            //             writeFile file: './tmpScripts/RunDeploy.sh', text: script_runDeploy
            //         }
            //         powershell ".\\tmpScripts\\RunDeploy.sh '${env.BUILD_BRANCHLEVEL}.sh'"
            //         script{
            //             if(fileExists('./customScript/postDeploy.sh')){
            //                 powershell '.\\customScript\\postDeploy.sh'
            //             }
            //         } 
            //     }
            // }
        }
        post{
            always{
                sh "ls -l"
                archiveArtifacts artifacts: '*.zip', fingerprint: true
            }
            success{
                bitbucketStatusNotify(buildState: 'SUCCESSFUL', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
            }
            failure{
                bitbucketStatusNotify(buildState: 'FAILED', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
            }
        }
    }
}
