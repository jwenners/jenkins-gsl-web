// vars/npmPipe.groovy

def call(String agentLabel, String nodeTool,String productName, String majorVersion, String minorVersion, int buildTimeout, int testTimeout ,Closure setEnv){
    pipeline{
        agent{
            label agentLabel
        }
        tools {nodejs "${nodeTool}"}
        options {
            buildDiscarder(logRotator(numToKeepStr: '6', artifactNumToKeepStr: '6'))
            skipStagesAfterUnstable()
            timestamps()
        }
        environment {
            BUILD_REPO_URL="${env.GIT_URL}"
            BUILD_REPO_SLUG = version_web.repoSlug("${env.GIT_URL}")
            BUILD_PRODUCTNAME = "${productName}"
            BUILD_MAJOR_VERSION = "${majorVersion}"
            BUILD_MINOR_VERSION = "${minorVersion}"
            BUILD_COMPANY = "FLIR Systems"
            BUILD_TRADEMARK = "FLIR® is a registered trademark of FLIR Systems."
            BUILD_STABILITY = "${version_web.getStabilityLevel("${env.BRANCH_NAME}")}"
            BUILD_CURRENTNUM = VersionNumber( projectStartDate: '', versionNumberString: '${BUILDS_TODAY}', versionPrefix: "", worstResultForIncrement: 'SUCCESS' )
            BUILD_BRANCHLEVEL = "${version_web.branchLevel("${env.BRANCH_NAME}")}"
            BUILD_REVISION = "${version_web.dayOfYear()}"
            BUILD_VERSION = "${version_web.buildNum("${env.BRANCH_NAME}", majorVersion, minorVersion, "${env.BUILD_CURRENTNUM}")}"
        }
        stages{
            stage('pre-build'){
                steps{
                    script{
                        if(fileExists('./customScript/preBegin.ps1')){
                            powershell '.\\customScript\\preBegin.ps1'
                        }
                    }
                    bat "set"
                    bitbucketStatusNotify(buildState: 'INPROGRESS', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
                    bat 'mkdir .\\tmpScripts'
                    script{
                        setEnv()
                        def script_functions = libraryResource 'flir/functions.ps1'
                        writeFile file: './tmpScripts/functions.ps1', text: script_functions
                        def script_setVersion = libraryResource 'flir/setVersion.ps1'
                        writeFile file: './tmpScripts/setVersion.ps1', text: script_setVersion
                    }
                    script{
                        if(fileExists('./customScript/postBegin.ps1')){
                            powershell '.\\customScript\\postBegin.ps1'
                        }
                    }        
                }
            }
            stage('build'){
                steps{
                    script{
                        if(fileExists('./customScript/preBuild.ps1')){
                            powershell '.\\customScript\\preBuild.ps1'
                        }
                    }
                    script{
                        def script_NewInstall = libraryResource 'flir/yarn/NewInstall.ps1'
                        writeFile file: './tmpScripts/NewInstall.ps1', text: script_NewInstall
                        def script_runBuild = libraryResource 'flir/yarn/RunBuild.ps1'
                        writeFile file: './tmpScripts/RunBuild.ps1', text: script_runBuild
                    }
                    timeout(time: buildTimeout, unit: 'MINUTES'){
                        powershell ".\\tmpScripts\\setVersion.ps1 ${env.BUILD_VERSION} ${env.GIT_COMMIT}"
                        powershell ".\\tmpScripts\\NewInstall.ps1"
                        powershell ".\\tmpScripts\\RunBuild.ps1 -BranchLevel '${env.BUILD_BRANCHLEVEL}.ps1'"
                    }
                    script{
                        if(fileExists('./customScript/postBuild.ps1')){
                            powershell '.\\customScript\\postBuild.ps1'
                        }
                    } 
                }
            }
            stage('test'){
                steps{
                    script{
                        if(fileExists('./customScript/preTest.ps1')){
                            powershell '.\\customScript\\preTest.ps1'
                        }
                    }
                    script{
                        def script_runTest = libraryResource 'flir/yarn/RunTest.ps1'
                        writeFile file: './tmpScripts/RunTest.ps1', text: script_runTest
                    }
                    timeout(time: testTimeout, unit: 'MINUTES'){
                        powershell ".\\tmpScripts\\RunTest.ps1 -BranchLevel '${env.BUILD_BRANCHLEVEL}.ps1'"
                    }
                    script{
                        if(fileExists('./customScript/postTest.ps1')){
                            powershell '.\\customScript\\postTest.ps1'
                        }
                    } 
                }
            }
            stage('deploy'){
                steps{
                    script{
                        if(fileExists('./customScript/preDeploy.ps1')){
                            powershell '.\\customScript\\preDeploy.ps1'
                        }
                    }
                    script{
                        def script_runDeploy = libraryResource 'flir/npm/RunDeploy.ps1'
                        writeFile file: './tmpScripts/RunDeploy.ps1', text: script_runDeploy
                    }
                    powershell ".\\tmpScripts\\RunDeploy.ps1 -BranchLevel '${env.BUILD_BRANCHLEVEL}.ps1'"
                    script{
                        if(fileExists('./customScript/postDeploy.ps1')){
                            powershell '.\\customScript\\postDeploy.ps1'
                        }
                    } 
                }
            }
        }
        post{
            always{
                archiveArtifacts artifacts: '*.zip', fingerprint: true
            }
            success{
                bitbucketStatusNotify(buildState: 'SUCCESSFUL', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
            }
            failure{
                bitbucketStatusNotify(buildState: 'FAILED', commitId: "${env.GIT_COMMIT}", repoSlug: "${env.BUILD_REPO_SLUG}")
            }
        }
    }
}
